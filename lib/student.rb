class Student

  def initialize(first_name,last_name)
    @first_name = first_name
    @last_name = last_name
    @total_courses = []
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    [@first_name,@last_name].join(" ")
  end

  def courses
    @total_courses
  end

  def enroll(course)
    if !course.students.include?(self)
      self.courses.each do |c|
        raise_error if c.conflicts_with?(course)
      end
      course.students << self
      self.courses << course
    end
  end

  def course_load
    result = Hash.new(0)
    @total_courses.each do |course|
      result[course.department] += course.credits
    end
    result
  end
end
